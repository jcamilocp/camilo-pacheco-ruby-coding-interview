module Api
  class TweetsController < ApplicationController
    skip_before_action :verify_authenticity_token
    before_action :has_same_body_1_day_ago?, only: [:create]

    rescue_from ActiveRecord::RecordInvalid do |e|
      render json: { error: e.message }, status: :unprocessable_entity
    end

    TWEETS_PER_PAGE = 50

    def create
      return head :bad_request if params[:body].blank? || params[:user_id].blank?

      @tweet = Tweet.create!(
          user_id: params[:user_id],
          body: params[:body]
        )

      render json: @tweet
    end

    def index
      @tweets = Tweet.order("created_at ASC").limit(TWEETS_PER_PAGE).offset(offset)

      render json: @tweets
    end

    private

    def page_params
      params.permit(:page).merge(per_page: TWEETS_PER_PAGE)
    end

    def page
      params[:page]
    end

    def offset
      return 0 if !page || page == 1

      page * (page.to_i - 1)
    end

    def has_same_body_1_day_ago?
      same_tweet = Tweet.where(
        'body = ? AND created_at >= ?',
        params[:body], 1.day.ago
      )

      render json: {}, status: :unprocessable_entity if same_tweet.present?
    end
  end
end