class Tweet < ApplicationRecord
  validates :body, length: { maximum: 180 }

  belongs_to :user
end
