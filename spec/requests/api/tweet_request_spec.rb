require 'rails_helper'

RSpec.describe "API Tweets", type: :request do
  describe "#create" do
    let(:response_body) { JSON.parse(response.body) }

    context 'with valid parameters' do
      let(:user1) { create(:user)}
      let(:valid_body) { 'This is a valid tweet' }

      it 'returns a successful response' do
        post api_tweets_path(user_id: user1.id, body: valid_body)

        expect(response).to have_http_status(:success)
      end

      it 'creates a new tweet' do
        expect {
          post api_tweets_path(user_id: user1.id, body: valid_body)
        }.to change(Tweet, :count).by(1)
      end
    end

    context 'With invalid parameters' do
      let(:user1) { create(:user)}

      context 'When the body is too long' do
        let(:invalid_body) { "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."}

        it 'returns an error response' do
          post api_tweets_path(user_id: user1.id, body: invalid_body)
          expect(JSON.parse(response.body)['error']).to be_truthy
        end

        it 'does not create a new tweet' do
          expect {
            post api_tweets_path(user_id: user1.id, body: invalid_body)
          }.to_not change(Tweet, :count)
        end
      end

      context 'When the tweet might be a duplicate' do
        let(:user1) { create(:user)}
        let(:valid_body) { 'This is a valid tweet' }

        it 'does not create a new tweet when there is one created with the same body' do
          create(:tweet, body: valid_body)

          expect {
            post api_tweets_path(user_id: user1.id, body: valid_body)
          }.to_not change(Tweet, :count)
        end
      end
    end
  end
end
